﻿using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEditor.iOS.Xcode;

// 參考
// https://support.unity.com/hc/en-us/articles/207942813-How-can-I-disable-Bitcode-support-
class DisableBitcode : IPostprocessBuildWithReport
{
    public int callbackOrder { get { return 0; } }
    public void OnPostprocessBuild(BuildReport report)
    {
        if (report.summary.platform == BuildTarget.iOS)
        {
            string projectPath = report.summary.outputPath + "/Unity-iPhone.xcodeproj/project.pbxproj";


            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);


            //Disabling Bitcode on all targets


            //Main
            string target = pbxProject.GetUnityMainTargetGuid();

            pbxProject.AddBuildProperty(target, "OTHER_LDFLAGS", "-v");

            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");


            //Unity Tests
            target = pbxProject.TargetGuidByName(PBXProject.GetUnityTestTargetName());
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");


            //Unity Framework
            target = pbxProject.GetUnityFrameworkTargetGuid();
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");


            pbxProject.WriteToFile(projectPath);
        }
    }
}