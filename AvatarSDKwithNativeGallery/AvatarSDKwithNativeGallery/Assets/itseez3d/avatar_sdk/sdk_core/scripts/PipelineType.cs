﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

using System.Collections.Generic;
using UnityEngine;

namespace ItSeez3D.AvatarSdk.Core
{
	public enum PipelineType
	{
		/// <summary>
		/// animated_face pipeline, base/legacy subtype
		/// </summary>
		FACE,

		/// <summary>
		/// animated_face pipeline, indie/legacy_styled subtype. Is used for the cartoonish stylization.
		/// </summary>
		STYLED_FACE,

		/// <summary>
		/// head_1.2 pipeline, base/mobile subtype
		/// </summary>
		HEAD
	}

	public static class PipelineTypeExtensions
	{
		public static Dictionary<PipelineType, string> pipelineTypeNameDict = new Dictionary<PipelineType, string>()
		{
			{ PipelineType.FACE, "animated_face"},
			{ PipelineType.STYLED_FACE, "animated_face"},
			{ PipelineType.HEAD, "head_1.2"}
		};

		public static Dictionary<PipelineType, string> pipelineSubtypeNameDict = new Dictionary<PipelineType, string>()
		{
			{ PipelineType.FACE, "base/legacy"},
			{ PipelineType.STYLED_FACE, "indie/legacy_styled"},
			{ PipelineType.HEAD, "base/mobile"}
		};

		public static string GetPipelineTypeName(this PipelineType pipelineType)
		{
			return pipelineTypeNameDict[pipelineType];
		}

		public static string GetPipelineSubtypeName(this PipelineType pipelineType)
		{
			return pipelineSubtypeNameDict[pipelineType];
		}

		/// <summary>
		/// Hash function for storing parameters of different pipelines in cache
		/// </summary>
		public static string ParametersSubsetHash(this PipelineType pipelineType, ComputationParametersSubset parametersSubset)
		{
			return string.Format("{0} {1}", parametersSubset.ToString(), pipelineType.ToString());
		}
	}
}
