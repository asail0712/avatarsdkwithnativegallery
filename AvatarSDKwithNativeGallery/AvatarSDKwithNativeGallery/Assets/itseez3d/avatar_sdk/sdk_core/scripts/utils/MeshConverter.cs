﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ItSeez3D.AvatarSdk.Core
{
	public enum MeshFileFormat
	{
		OBJ,
		FBX
	}

	public interface IMeshConverter
	{
		bool IsObjConvertEnabled { get; }

		bool IsFBXExportEnabled { get; }

		IntPtr CreateMeshObject(string plyModelFile, string templateModelFile, string textureFile);

		IntPtr CreateMeshObjectWithTexture(string plyModelFile, string templateModelFile, IntPtr textureImage, int textureWidth, int textureHeight);

		int ReleaseMeshObject(IntPtr mesh);

		int MergeMeshObjects(IntPtr dstMesh, IntPtr srcMesh);

		int ApplyBlendshapesToMeshObject(IntPtr mesh, string blendshapesDir, string blendshapesNamesWithWeights);

		int LoadBlendshapesForMeshObject(IntPtr mesh, string blendshapesDir);

		int SaveMeshToObj(IntPtr mesh, string objModelFile, string textureFile);

		int SaveMeshToFbx(IntPtr mesh, string fbxModelFile, string textureFile);
	}
}
